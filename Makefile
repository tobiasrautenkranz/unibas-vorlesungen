data_dir := raw_data
objects := $(patsubst $(data_dir)/%.pdf, $(data_dir)/%.dat, $(wildcard $(data_dir)/*.pdf))

data_file := 2019fs.csv

all: plots

.PHONY: download
download:
	mkdir -p $(data_dir)
	./download_pdfs.sh $(data_dir)

$(data_dir)/%.dat: $(data_dir)/%.pdf
	pdftotext -l 1 $< - | head -n 2 > $@
	pdftotext $< - \
		| egrep "[A-Z][a-z] [0-9][0-9]:[0-9][0-9]-[0-9][0-9]:[0-9][0-9]" -o \
		| sed 's/[ -]/,/g' >> $@

$(data_file): $(objects)
	./data_to_csv.r $(data_dir) $@

.PHONY: plots
plots: $(data_file) *.r
	./p.r $<

plots.pdf: $(filter-out plots.pdf, $(wildcard *.pdf))
	pdftk $^ cat output $@


.PHONY: clean
clean:
	rm -f -- $(objects)
	rm -f -- *png
