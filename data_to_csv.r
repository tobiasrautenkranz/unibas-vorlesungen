#!/usr/bin/Rscript

args <- commandArgs(trailingOnly=TRUE)
if (length(args) != 2) {
    stop("Error: Required Argument: DATA_DIRECTORY OUTPUT_FILE", call.=FALSE)
}
data_directory <- args[1]
output_csv <- args[2]

source("data.r")

rdata <- read_data_files(data_directory)
write_csv(rdata, output_csv)
