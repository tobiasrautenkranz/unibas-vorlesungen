library("dplyr")
library("purrr")
library("forcats")
library("ggplot2")
library("viridis")


#
# utils
#

hm_str <- function(time) {
  format(as.POSIXct(time, tz="GMT"), "%H:%M")
}

#
# Score (for order)
#
# higher = worse (less free time)

# time
# (all times are in seconds)

morning_score <- function (begin_time) {
  t <- as.double(begin_time, units="secs")
  earliest <- 8*60*60
  half <- 10*60*60
  k <- log(2)/(half - earliest)

  exp(-k*(t - earliest))
}

lunch_score <- function (begin, end) {
  begin <- as.double(begin, units="secs")
  end <- as.double(end, units="secs")

  mid <- 12.5 * 60 * 60
  sd <- 15 * 60
  pnorm(end, mid, sd) - pnorm(begin, mid, sd)
}

evening_score <- function (end_time) {
  t <- as.double(end_time, units="secs")
  latest <- 20*60*60
  half <- 18*60*60
  k <- log(2)/(latest-half)

  exp(-k*(latest - t))
}

score_times <- function(data) {
  data %>% summarize(morning = mean(morning_score(begin)),
                     lunch = mean(lunch_score(begin, end)),
                     evening = mean(evening_score(begin)))
}

# week score

long_weekend_score <- function(day) {
  map_dbl(day, long_weekend_score_impl)
}

long_weekend_score_impl <- function(day) {
  if (day == "Sa"|| day == "So") {
    return(1)
  } else if (day == "Mo" || day == "Fr") {
    return(0.5)
  } else if (day == "Di" || day == "Do") {
    return(0.25)
  }
  stopifnot(day == "Mi")
  return(0.1)
}

#
# plot
#

# color per faculty
fill_colors <- function(data, group, order) {
  group <- enquo(group)
  order <- enquo(order)

  faculties <- data %>% ungroup %>% count(faculty) %>% mutate(color=viridis(length(faculty)))

  if (rlang::quo_is_missing(order)) {
    colors <- data %>% count(!!group) %>% left_join(faculties, by="faculty")
  } else {
    colors <- data %>% group_by(faculty, !! group) %>% summarise(!!order:=mean(!!order)) %>%
             left_join(faculties, by="faculty") %>% arrange(!!order)
  }
  colors$color
}

plot_time_raster <- function(d, group=degree, order) {
  group <- enquo(group)
  order <- enquo(order)

  if (rlang::quo_is_missing(order)) {
    y_data <- group
    colors <- fill_colors(d, !! group)
  } else {
    d <- d %>% arrange(!!order)
    y_data <- expr(fct_reorder(!! group, !! order))

    colors <- fill_colors(d, !! group, !! order)
  }

  ggplot(d, aes(y = !!y_data,
                x=idx, fill=bins, label=!! group, color=faculty)) + geom_raster() +
    scale_fill_gradient2(limits=c(0,1)) +
    scale_x_time(labels=hm_str, breaks=seq(8,20,2)*60*60, expand=c(0,0)) +
    scale_y_discrete(expand=c(0,0)) +
    labs(fill="ratio", y=NULL, x=NULL) +
    theme(axis.text.y = element_text(colour=colors))
}

plot_weekdays <- function(data, group=degree) {
  group <- enquo(group)
  a <- data %>% group_by(faculty, !! group, day) %>% summarise(n=n()) %>% mutate(f = n/sum(n))
  a <- a %>% left_join(data %>% group_by(!! group) %>%
                       summarise(score=mean(long_weekend_score(day))),
                     by = quo_name(group))
  colors <- fill_colors(a, !! group, score)
  ggplot(a, aes(x=day,y=fct_reorder(!! group, score), fill=f)) +
    geom_raster() +
    scale_fill_gradient2(limits=c(0,1)) +
    scale_x_discrete(expand=c(0,0)) +
    labs(fill="ratio", y=NULL, x=NULL) +
    theme(axis.text.y = element_text(colour=colors))
}

# faculty

plot_time_raster_faculty <- function(bined_data) {
  # use already relative bined data to give equal weight to all degrees
  d <- bined_data %>% group_by(faculty, idx) %>% summarise(bins=mean(bins))

  plot_time_raster(d, group=faculty)
}

plot_weekdays_faculty <- function(data) {
  plot_weekdays(data, faculty)
}

#
# score
#
plot_score <- function(data, score) {
  score <- enquo(score)
  ggplot(data %>% ungroup, aes(x=fct_reorder(degree, !!score), y=!!score, fill=faculty)) +
    geom_col() + coord_flip() +
    scale_fill_viridis_d() +
    labs(x=NULL, y="chill", fill=NULL)
}

