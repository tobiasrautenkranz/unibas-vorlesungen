library("readr")
library("dplyr")

read_data_file <- function(filename) {
  faculty <- read_lines(filename, n_max=1)
  title <- read_lines(filename, skip=1, n_max=1)
  data <- read_csv(filename, skip=2, col_names=c("day", "begin", "end"), col_types="ctt")
  mutate(data, degree = title, faculty = faculty)
}

read_data_files <- function(path=".") {
  bind_rows(lapply(list.files(path=path, pattern="*.dat", full.names="true"), read_data_file))
}

factor_data <- function(data) {
  data$degree <- factor(data$degree)
  data$day <- factor(data$day, levels = c("Mo", "Di", "Mi", "Do", "Fr", "Sa", "So"))

  data
}
read_data_file_csv <- function(filename) {
  factor_data(factor_data(read_csv(data_file, col_types="cttcc")))
}
