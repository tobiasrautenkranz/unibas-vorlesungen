#!/bin/bash
set -euo pipefail

if [ $# -ne 1 ]; then
    echo "usage: $0 OUTPUT_DIRECTORY" >&2
    exit 1
fi

cd "$1"

function extract_ids {
  grep -o '\"key\":[0-9]*' | cut -c 7-
}

base_url="https://vorlesungsverzeichnis.unibas.ch"
tree_url="${base_url}/components/hierarchie.cfc?method=getTree"
tree_ids=$(curl -sS "${tree_url}" | extract_ids)

subtree_url="${base_url}/components/hierarchie.cfc?method=getSubTree"

function get_subtree {
  curl -sS "${subtree_url}&level=$1&parentid=$2"
}

function extract_pdf_urls {
  grep -o "/pdf/[0-9]*/[[:alnum:]_]*_k.pdf" || return 0
}

function get_pdfs {
  for pdf_url in "$@"; do
    wget -nc "${base_url}${pdf_url}"
    #wget -Nq "${base_url}${pdf_url}"
  done
}

function get_all_pdfs {
  level=$1
  shift
  echo "ids: $@"
  for id in "$@"; do
    pdf_urls=$(get_subtree $level $id | extract_pdf_urls)
    #echo "id: ${id} pdfs: ${pdf_urls}"
    if [ -n "$pdf_urls" ]; then
      get_pdfs $pdf_urls
    else
      echo "recurse $id"
      get_all_pdfs $(($level+1)) $(get_subtree $level $id | extract_ids)
    fi
    level=$1
  done
}

get_all_pdfs 1 $tree_ids

#for id in $tree_ids; do
#  pdfs=$(curl -s "${subtree_url}${id}" | grep -o "/pdf/[0-9]*/[[:alnum:]_]*_k.pdf")
#  if [ -n "$pdfs" ]; then
#    echo "id: ${id} pdfs: ${pdfs}"
#    get_pdfs $pdfs
#  else
#    s_ids=$(curl -s "${subtree_url}${id}" | grep -o '\"key\":[0-9]*' | cut -c 7-)
#    for i in $s_ids; do
#  fi
#done
