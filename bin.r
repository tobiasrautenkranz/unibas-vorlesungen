library("tidyr")
library("hms")

bin_range <- function(df, lower, upper, bin_width, origin=NULL)
{
  lower <- enquo(lower)
  upper <- enquo(upper)

  range <- df %>% ungroup %>% summarise(min=min(!! lower),max=max(!! upper))

  rrange <- df %>% ungroup %>% summarise(min=min(!! upper),max=max(!! lower))
  stopifnot(rrange$max <= range$max)
  stopifnot(rrange$min >= range$min)

  #  < width>
  # |   1    |    2     |    3    |
  #
  # ^                             ^
  # min                         max
  if (is.null(origin)) {
    n_bins <- ceiling(as.double(range$max - range$min, units="secs") / bin_width)
    origin <-range$min-bin_width/2
  } else {
    n_bins <- ceiling(as.double(range$max - origin, units="secs") / bin_width)
  }

  summarize(df, bins=to_bins(n_bins, origin, bin_width, !! lower, !! upper),
            idx=bin_indices(n_bins, origin, bin_width)) %>%
    unnest %>%
    mutate(idx = hms(idx))
}

bin_indices <- function(count, origin, width) {
  list(seq(0, count-1)*width + as.double(origin, units="secs"))
}

to_bins <- function(count, origin, width, begin, end)
{
  first_bin <- ceiling(as.double(begin - origin, units="secs") / width)
  last_bin <- floor(as.double(end - origin, units="secs") / width)

  stopifnot(first_bin >= 1)
  stopifnot(last_bin <= count)

  bins <- integer(count)

  for (i in seq_along(first_bin)) {
    for (j in seq(first_bin[i], last_bin[i])) {
      bins[j] <- bins[j]+1
    }
  }
  bins <- bins / length(first_bin)

  #print(bins)
  #print(x)

  #walk(begin, end, function(a, b) set_bins(bins, origin, width, a, b))
  #print(bins)
  list(bins)
}

set_bins <- function(bins, origin, width, begin, end)
{
  first_bin <- as.double(begin - origin, units="secs")   %/% width
  last_bin <- as.double(end - origin, units="secs")  %/% width
  inc(bins[first_bin]) <- 1
  inc(bins[last_bin]) <- 1
  bins[3] <- 10000
  #for (i in first_bin:last_bin) {
  #  inc(bins[i]) <- 1
  #}
  bins
}
