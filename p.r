#!/usr/bin/Rscript

library("stringr")

args <- commandArgs(trailingOnly=TRUE)
if (length(args) > 1) {
    stop("Error: Required Argument: DATA_FILE", call.=FALSE)
}

if(length(args) == 1) {
  data_file <- args[1]
} else if (!exists("data_file")) {
  data_file <- "rdata.csv"
}

source("data.r")
source("bin.r")
source("plots.r")

rdata <- read_data_file_csv(data_file)

## remove degrees with few courses
n_course_cutoff <- 10
data <- rdata %>% add_count(degree) %>% filter(n > n_course_cutoff) %>% select(-n)

bachelor <- data %>% ungroup %>% filter(str_detect(degree, "Bachelor")) %>%
  mutate(studienfach=str_detect(degree, "Bachelor Studienfach"))  %>%
  mutate(degree=str_replace(degree, "Bachelor Studienfach - ", " ")) %>% # add a space to fix sorting in facet
  mutate(degree=str_replace(degree, "Bachelor |Bachelorstudium: ", ""))

bin_data <- function(data, ...) {
  extra_groups <- enquos(...)
  data %>% group_by(faculty, degree, !!!extra_groups) %>%
  bin_range(begin, end, 15*60, 7.75*60*60) %>%
  inner_join(data %>% group_by(faculty, degree) %>% score_times,
             by=c("faculty", "degree"))
}

# time
bachelor_binned <- bin_data(bachelor, studienfach)

facet_bachelor <- function() {
  list(facet_wrap(~studienfach, dir="v", scale="free_y",
                  labeller=as_labeller(c("FALSE"="Bachelor", "TRUE"="Bachelor Studienfach"))))
}

pdf("bachelor.pdf", height=10,width=10)
print(plot_time_raster(bachelor_binned, order=morning) + ggtitle("Morgen") + facet_bachelor())

print(plot_time_raster(bachelor_binned, order=evening) + ggtitle("Abend") + facet_bachelor())

print(plot_time_raster(bachelor_binned, order=lunch) + ggtitle("Mittag") + facet_bachelor())

# week
pdf("bachelor_week.pdf", height=8,width=10)
print(plot_weekdays(bachelor %>% filter(studienfach == TRUE)))
print(plot_weekdays(bachelor %>% filter(studienfach == FALSE)))
dev.off()



pdf("chill_bachelor.pdf", width=10)

chill_ranking <- function(data, ...) {
  groups <- enquos(...)
  score_w <- data %>% group_by(!!! groups) %>% summarize(weekend=mean(long_weekend_score(day)))
  score_t <- data %>% group_by(!!! groups) %>% score_times
  ranking <- left_join(score_w, score_t, by=map_chr(groups, quo_name)) %>%
    group_by(!!! groups) %>%
    summarize(score = 1/sum(weekend + 0.5*morning + 0.25 * evening + 0.125 * lunch))
  ranking$score <- ranking$score/max(ranking$score)
  ranking$score <- ranking$score-min(ranking$score) ## FIXME do not use relative scaling

  ranking
}

print(plot_score(chill_ranking(bachelor, faculty, degree, studienfach), score) +
      guides(fill=FALSE) +
      facet_bachelor())
dev.off()

## faculty

pdf("by_department.pdf", width=10, height=3)
print(plot_time_raster_faculty(bin_data(data)))

print(plot_weekdays_faculty(data))
dev.off()
